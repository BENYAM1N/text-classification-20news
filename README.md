# Text-Classification-20news

Simple text classification project on 20news data, using word embedding methods for text representation and supervised machine learning algorithms to classify.

## Data Description
20news data consists of 20 different categories of news in the form of text which are divided into train and test set.


The number of news in train set is 11314 and the number of news in test set is 7532.




## Method
There are 3 main part in this project:

### Text Cleaning
Text cleaning module consists of
*  Word Tokenizing
*  Stopword Removal
*  Header Removal
*  Digit and Punctutaion Removal
*  Stemming

(text_cleaner.py is a multiprocess module written by me which uses NLTK as its backbone, more information about function and its usage is available on the text_cleaner.py)

### Word and Text Representation
Word representation methods are one-hot and Word2Vec.

In order to represent a news a simple average of normalized words' vector are computed.


### Classification Algorithm
Two algorithms are used in order to classify test news into their proper category.
*  KNN (K Nearest Neighbout)
*  SVM (Support Vector Machine)


## Result
